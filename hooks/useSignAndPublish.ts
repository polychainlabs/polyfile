import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import Cid from '~/common/Cid';
import Message from '~/common/Message';
import MsigSerialization from '~/common/MsigSerialization';
import SignedMessage from '~/common/SignedMessage';
import { WalletAddress } from '~/common/Wallet';
import { useLotusRpc, useGlifLotusRpc } from './useLotusRpc';
import useWallet from './useWallet';

type PoolPushFn = (address: WalletAddress, message: Message) => Promise<Cid>;

/**
 * Lifecycle hook for getting a message signed and published
 * 1. calculate gas
 * 2. sign
 * 3. publish
 */
export default function useSignAndPublish(): PoolPushFn {
  const readClient = useLotusRpc();
  const writeClient = useGlifLotusRpc();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const wallet = useWallet();

  return useCallback(
    async (address: WalletAddress, message: Message) => {
      if (!wallet) {
        throw new Error('Wallet required to sign');
      }

      const nonce = await readClient.mpoolGetNonce(message.from);
      message.nonce = nonce;

      const head = await readClient.chainHead();
      const gasEstimate = await readClient.gasEstimateMessageGas(message, head.Cids);

      message.gaspremium = BigInt(gasEstimate.GasPremium);
      message.gasfeecap = BigInt(gasEstimate.GasFeeCap);
      message.gaslimit = gasEstimate.GasLimit;

      const signSnackbarKey = enqueueSnackbar('Review transaction on ledger', {
        variant: 'info',
        persist: true,
        anchorOrigin: { horizontal: 'center', vertical: 'top' },
      });

      const serializedMessage = MsigSerialization.Message.ToBuffer(message);
      const signResponse = await wallet.sign(address, serializedMessage).finally(() => {
        closeSnackbar(signSnackbarKey);
      });

      const signedMessage: SignedMessage = {
        message,
        signature: {
          type: 1,
          data: Buffer.from(signResponse.signatureCompact),
        },
      };

      return writeClient.mpoolPush(signedMessage);
    },
    [closeSnackbar, enqueueSnackbar, readClient, writeClient, wallet],
  );
}
