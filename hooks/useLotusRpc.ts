import { useState } from 'react';

import LotusRpc from '~/services/LotusRpc';

export function useLotusRpc(): LotusRpc {
  const [rpc] = useState(
    new LotusRpc('https://public-api.filecoin-mainnet.nodes.unit410.com/rpc/v1'),
  );
  return rpc;
}

export function useGlifLotusRpc(): LotusRpc {
  const [rpc] = useState(new LotusRpc('https://api.node.glif.io'));
  return rpc;
}
